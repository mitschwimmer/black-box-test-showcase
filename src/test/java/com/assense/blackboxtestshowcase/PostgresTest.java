package com.assense.blackboxtestshowcase;

import com.assense.blackboxtestshowcase.domain.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {
        "spring.datasource.url=jdbc:tc:postgresql:14-alpine://testcontainers/workshop"
})
class PostgresTest {

    @Autowired
    private TestRestTemplate testRestTemplate; // Aufpassen, TestRestTemplate wirft keine Exceptions

    @Test
    void contextLoads() {
        // Wenn das hier funktioniert, startet unsere Applikation noch
    }

    @Test
    void getPersonByIdFailsWithoutId() {
        // when
        ResponseEntity<Person> person = testRestTemplate.getForEntity("/api/person", Person.class);

        // then
        assertThat(person.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void getPersonByIdReturnsValidPerson() {
        // when
        ResponseEntity<Person> person = testRestTemplate.getForEntity("/api/person&1", Person.class);

        // then
        assertThat(person.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(person.getBody()).isNotNull();
        assertThat(person.getBody().getFirstName()).isEqualTo("Joe");
    }
}