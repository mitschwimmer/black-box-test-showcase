package com.assense.blackboxtestshowcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlackBoxTestShowcaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlackBoxTestShowcaseApplication.class, args);
    }

}
