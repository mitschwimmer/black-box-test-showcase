package com.assense.blackboxtestshowcase.api;

import com.assense.blackboxtestshowcase.domain.Person;
import com.assense.blackboxtestshowcase.domain.PersonRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/persons")
public class ApiController {

    private final PersonRepository personRegistry;

    public ApiController(PersonRepository personRegistry) {
        this.personRegistry = personRegistry;
    }

    @GetMapping( "/person/{id}")
    Person getPersonById(@PathVariable Long id) {
        // hier würden wir natürlich DTO's zurückgeben, das spare ich mir hier mal
        return personRegistry.findById(id).orElseThrow();
    }

}
