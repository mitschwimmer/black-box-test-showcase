# "Black Box"-Test Showcase

## Was heisst hier "Black Box"?
Vor nicht allzu langer Zeit hätten wir vermutlich "System-Tests" dazu gesagt. Allerdings habe ich festgestellt, dass es
noch nicht mal im Kreise hauptberuflicher Tester ganz eindeutige Definitionen bzgl. der diversen Testarten zu geben
scheint.
Unter "Black Box" verstehe ich Tests, die für den eigentlichen Test die Details der Implementierung ignorieren und nur
Outputs aufgrund definierter Inputs abgleichen. Damit ist auch klar, dass sich diese Art Tests nicht für jede Software
eignet.

## Was bringen solche Tests
Sei es Wartung (wie Dependency Updates) oder Entwicklung neuer Features, als Entwickler muss ich wissen ob Regressionen
auftreten. Natürlich kann ich auch einen manuellen Testplan verfolgen. Doch das ist Zeit-intensiv und fehleranfällig. In
der Realität führt das dazu, das diese Tests seltener gemacht werden als gut wäre. Automatisierte Tests können von der
CI auf meinem Feature Branch ausgeführt werden oder in meiner IDE. Das geht vergleichsweise schnell und garantiert die
immer gleiche Testausführung.

## Tests sind Dokumentation
Werden Tests entlang fachlicher Anforderungen formuliert und sind nicht schlichte Permutationstests, dann dokumentieren 
sie nebenbei eben diese Anforderungen und die korrekte Nutzung der Inputs.

## Eigenheiten des hier gewählten Beispiels
Es handelt sich um einen API-Server mit REST-artigen Endpunkten. Damit eignet er sich hervorragend für Black-Box Tests.

## Warum keine In-Memory Datenbank
Für Tests dieser Art wird im Test gern H2 als schnellerer Datenbank In-Place-Ersatz verwendet. Mir ist das in der 
Vergangenheit schon auf die Füße gefallen, da H2 nicht dieselbe SQL Syntax verwendet wie eine echte
Postgres/Oracle/was-auch-immer.

## Nachteil von Testcontainers
Wie der Name schon verrät werden im Test Container verwendet. Hier halt Postgres. Das bedeutet für die CI-Umgebung je-
doch, das auf dem Test ausführenden Node geschachtelte Virtualisierung möglich sein muss. Das ist nicht überall gegeben
und ich weiß nicht wie es in unserem Test-Cluster damit aussieht. Ggf. muss es dann doch H2 sein oder es braucht eine
Postgres-Instanz für Tests. CI macht halt Arbeit ;-)

## Was dem Beispiel noch fehlt
- [ ] Datenbankschema und Fixtures - noch schlägt der Test für den echten Datenbezug fehl
- [ ] Flyway für die Versionierung von Datenbank-Skripten